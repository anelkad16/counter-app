package com.example.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder

class CounterService : Service() {

    var counter = 0
        private set

    override fun onBind(intent: Intent): IBinder = CounterBinder()

    fun incrementCounter() {
        counter++
    }

    fun decrementCounter() {
        counter--
    }

    inner class CounterBinder : Binder() {
        fun getService(): CounterService {
            return this@CounterService
        }
    }
}
